﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody2D))]

public class DinoBehaviour : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    public float fuerzaSalto = 5.0f;
    public bool isGrounded = false;
    public GameObject graphics;
    [SerializeField] private GameObject graficos;
    [SerializeField] private Animator animator;
    private float escalactual;
    private bool isDead = false;
    //[SerializeField] 
    
    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        if (Math.Abs(rb2d.velocity.x) > 0.01f)
        {
            animator.SetFloat("velocidadX", Math.Abs(rb2d.velocity.x));
        }

        if (isDead) { return; }
    }

    private void Update()
    {
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);

        if (isDead) { return; }

        if (jump && isGrounded)
        {
            Debug.LogError("Salta");
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        escalactual = graphics.transform.localScale.x;

        if (move > 0 && escalactual<0)
        {
            graphics.transform.localScale = new Vector3(1, 1, 1);
        }else if(move < 0 && escalactual > 0)
        {
            graphics.transform.localScale = new Vector3(-1, 1, 1);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.LogError("Colisiono");
        if (collision.gameObject.tag == "Muerte")
        {
            animator.SetTrigger("muerte");
            isDead = true;
        }else if (collision.gameObject.tag == "PowerUp")
        {
            Debug.Log("Has pillado una vida");
            maxS *= 4;
            Destroy(collision.gameObject);
        }else if (collision.gameObject.tag == "Trampolin")
        {
            rb2d.AddForce(Vector2.right * move * maxS*4);
        }
    }

    //public void Sonidomuerte()

    

}


