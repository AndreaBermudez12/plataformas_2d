﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrullero : MonoBehaviour
{
    [SerializeField] Transform[] posiciones;
    int posactual = 0;

    private void Update()
    {
        if (Vector3.Distance(posiciones[posactual].position, transform.position) > 1.0f)
        {
            posactual++;
            if (posactual >= posiciones.Length)
            {
                posactual = 0;
            }
        }
    }
}
